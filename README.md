VRND Capstone by Xavi Heras

Unity version: 2017.1.0p4
Headset: HTC VIVE (not multi platform support)
Test specs: GTX 1080 - i7-7700k

Controls:
	- trackpads : teleport
	- triggers : grab/release/use objects


For this project I wanted to create a small puzzle game in a mysterious island. This experience tries to convey various emotions:

	- Solitude: being alone in the middle of the sea.
	- Discovery: explore the secrets of the mysterious island
	- Joy: look around to understand how to solve puzzles 


Video URL:

	Walkthrought video : https://youtu.be/oNI6LjWOeMI

	Achievements video : https://youtu.be/RXVMGixfMeg


List of Achievements:

	Fundamentals
		Scale achievement (100 points)
		Animation achievement (100 points)
		Lightning achievement (100 points)
		Locomotion achievement (100 points)
		Physics achievement (100 points) 

		Total fundamentals 500 points

	Completeness
		Gamification achievement (250 points) 
		Diegetic UI achievement (250 points) 
		3D Modeling achievement (250 points) 

		Total completness 750 points

	Challenges
		User testing achivement (500 points) 
			test 1 - throw puzzle blocks
			test 2 - limit teleport area

		Total challenges 500 points


Total achievements points : 1750