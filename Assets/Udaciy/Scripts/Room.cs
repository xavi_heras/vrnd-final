﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {
	public GameObject room;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider other){
		Debug.Log (other.tag);
		if (other.CompareTag ("PlayerEyes")) {
			
			room.SetActive (false);
			Destroy (other.gameObject);
		}

	}
}
