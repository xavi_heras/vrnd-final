﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Valve.VR.InteractionSystem;

public class Island : MonoBehaviour {
	public GameObject cameraRig;
	public Transform locationEntrance;
	public Transform locationWheel;
	public Transform locationBow;
	public Transform locationStack;


	public CaveDoor door;
	public GameObject doorRight;
	public GameObject doorLeft;
	public GameObject doorWalls;

	public GameObject sun;
	public GameObject water;
	public GameObject sand;

	public GameObject portalLight;

	public Material cloudLow;
	public float amountLow;
	public float alphaLow;
	public Color colorLow;

	public GameObject clouds;
	public Material cloudHigh;
	public float amountHigh;
	public float alphaHigh;
	public Color colorHigh;

	public GameObject pathIsland;
	public GameObject pathPortal;

	public WheelPuzzleController puzzleWheel;
	public BowPuzzle puzzleBow;
	public StackPedestalArea puzzleStack;

	public AudioSource seaSound;
	public SoundPlayOneshot openDoorSound;
	public AudioSource portalSound;

	public Light myLight;
	public Light portalLightE;
	public Light portalLightE2;

	// Use this for initialization
	void Start () {
		pathPortal.SetActive (false);

		cloudLow.SetFloat ("_Density", amountLow);
		cloudHigh.SetFloat ("_Density", amountHigh);

		Color col = new Color ();

		/*
		col =  new Color (cloudLow.color.r, cloudLow.color.g, cloudLow.color.b, alphaLow);
		cloudLow.SetColor ("_CloudColor", Color.white);
		col = new Color (cloudHigh.color.r, cloudHigh.color.g, cloudHigh.color.b, alphaHigh);
		cloudHigh.SetColor("_CloudColor", Color.white);
		*/
		col = new Color (255f, 255f, 255f, alphaLow/100);
		cloudLow.SetColor ("_CloudColor", col);

		col = new Color (255f, 255f, 255f, alphaHigh/100);
		cloudHigh.SetColor ("_CloudColor", col);

	}
	
	// Update is called once per frame
	void Update () {
		//CheckKeyboardInput ();
		CheckDoor ();
	}

	void CheckDoor(){
		if (door.playerInFrontOfDoor && puzzleWheel.puzzleComplete && puzzleWheel.puzzleComplete && puzzleStack.puzzleComplete) {
			if (door.closed) {


				float seaVol;
				seaVol = seaSound.volume;
				Tweener tweenerSeaVol = DOTween.To (() => seaVol, x => seaVol= x, 0, 6f);
				tweenerSeaVol.OnUpdate(() => {
					seaSound.volume = seaVol;
				});


				myLight.enabled = false;
				puzzleWheel.DisableLight ();
				puzzleBow.DisableLight ();
				puzzleStack.DisableLight ();

				doorWalls.SetActive (true);
				pathIsland.SetActive (false);
				pathPortal.SetActive (true);

				door.closed = false;
				doorRight.transform.DORotate (new Vector3 (0f,75f,0f), 12f);
				doorLeft.transform.DORotate (new Vector3 (0f,-75f,0f), 12f).OnComplete(() => {
					//sand.SetActive(false);
				});

				if ( openDoorSound!= null )
				{
					openDoorSound.Play();
				}

				sun.transform.DORotate (new Vector3 (-68f,173f,246f), 12f);
				DOTween.To (() => sun.GetComponent<Light> ().intensity, x => sun.GetComponent<Light> ().intensity = x, 0f, 18f);
				water.transform.DOMoveY (-3f, 10f);

				//cloudLow.DOColor (new Color (cloudLow.color.r, cloudLow.color.g, cloudLow.color.b, 0), 10f);
				//cloudHigh.DOColor (new Color (cloudHigh.color.r, cloudHigh.color.g, cloudHigh.color.b, 0), 10f);


				colorLow = cloudLow.GetColor ("_CloudColor");
				Tweener tweenerLow = DOTween.To (() => colorLow, x => colorLow= x, new Color(colorLow.r,colorLow.g,colorLow.b,0f), 6f);
				tweenerLow.OnUpdate(() => {
					cloudLow.SetColor ("_CloudColor", colorLow);
				});

				colorHigh = cloudHigh.GetColor ("_CloudColor");
				Tweener tweenerHigh = DOTween.To (() => colorHigh, x => colorHigh = x, new Color(colorHigh.r,colorHigh.g,colorHigh.b,0f) , 6f);
				tweenerHigh.OnUpdate(() => {
					cloudHigh.SetColor ("_CloudColor", colorHigh);
				});
				/*
				tweenerHigh.OnComplete(() => {
					Destroy(clouds);	
				});
				*/

				portalLight.transform.DOShakePosition (.1f, .5f, 10, 90f).SetLoops (-1);


				float portalVol;
				portalVol = 0;
				Tweener tweenerPortalVol = DOTween.To (() => portalVol, x => portalVol= x, .6f, 6f);
				tweenerPortalVol.OnUpdate(() => {
					portalSound.volume = portalVol;
				});

				portalLightE.enabled = true;
				portalLightE2.enabled = true;
			}
		}
	}

	void CheckKeyboardInput(){
		if (Input.GetKeyDown (KeyCode.Alpha1))
			cameraRig.transform.position = locationEntrance.transform.position;
		if (Input.GetKeyDown (KeyCode.Alpha2))
			cameraRig.transform.position = locationBow.transform.position;
		if (Input.GetKeyDown (KeyCode.Alpha3))
			cameraRig.transform.position = locationWheel.transform.position;
		if (Input.GetKeyDown (KeyCode.Alpha4))
			cameraRig.transform.position = locationStack.transform.position;
		if (Input.GetKeyDown (KeyCode.O))
			OpenDoor ();
	}

	void OpenDoor(){


	}
}
