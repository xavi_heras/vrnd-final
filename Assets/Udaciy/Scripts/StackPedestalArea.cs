﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Valve.VR.InteractionSystem;

public class StackPedestalArea : MonoBehaviour {
	public bool puzzleComplete = false;
	public GameObject totem;
	public GameObject totem_rect;
	public GameObject totem_cube;
	public GameObject totem_tri;

	public SoundPlayOneshot totemSound;

	public GameObject rectangle_toy;
	public GameObject cube_toy;
	public GameObject triangle_toy;


	public GameObject mark;
	public Light myLight;
	public Light myLight2;
	public GameObject myTeleport;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//if (Input.GetKeyDown (KeyCode.R))CompletePuzzle ();
			
	}

	void OnTriggerStay (Collider other){
		if (!other.attachedRigidbody.isKinematic && other.attachedRigidbody.velocity == Vector3.zero) {
			CompletePuzzle ();
		}

	}

	private void CompletePuzzle(){
		if (!puzzleComplete) {
			Debug.Log ("stay stack  reach goal!");
			puzzleComplete = true;
			mark.SetActive (false);

			Destroy (rectangle_toy.GetComponent<Valve.VR.InteractionSystem.Throwable> ());
			Destroy (rectangle_toy.GetComponent<Valve.VR.InteractionSystem.InteractableHoverEvents> ());
			Destroy (rectangle_toy.GetComponent<Valve.VR.InteractionSystem.Interactable> ());

			Destroy (cube_toy.GetComponent<Valve.VR.InteractionSystem.Throwable> ());
			Destroy (cube_toy.GetComponent<Valve.VR.InteractionSystem.InteractableHoverEvents> ());
			Destroy (cube_toy.GetComponent<Valve.VR.InteractionSystem.Interactable> ());

			Destroy (triangle_toy.GetComponent<Valve.VR.InteractionSystem.Throwable> ());
			Destroy (triangle_toy.GetComponent<Valve.VR.InteractionSystem.InteractableHoverEvents> ());
			Destroy (triangle_toy.GetComponent<Valve.VR.InteractionSystem.Interactable> ());

			totem.transform.DOMoveY (-9f, 18f);
			totem_rect.transform.DOShakePosition (18f, .02f, 5, 5f);
			totem_cube.transform.DOShakePosition (18f, .02f, 5, 5f);
			totem_tri.transform.DOShakePosition (18f, .02f, 5, 5f);

			if ( totemSound != null )
			{
				totemSound.Play();
			}



			myTeleport.GetComponent<Valve.VR.InteractionSystem.TeleportPoint> ().markerActive = false;
		}

	}


	public void DisableLight(){
		myLight.enabled = false;
		myLight2.enabled = false;
	}
}
