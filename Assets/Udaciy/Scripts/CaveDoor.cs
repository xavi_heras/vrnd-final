﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveDoor : MonoBehaviour {
	public bool playerInFrontOfDoor = false;
	public bool closed = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay (Collider other){

		if (other.CompareTag ("MainCamera")) {
			playerInFrontOfDoor = true;
		}

	}


	void OnTriggerExit (Collider other){

		if (other.CompareTag ("MainCamera")) {
			playerInFrontOfDoor = false;
		}

	}

}
