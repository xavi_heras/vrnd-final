﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BowPuzzle : MonoBehaviour {
	public bool puzzleComplete = false;
	public BoxCollider safeZone;

	public GameObject pedestalCube;
	public GameObject pedestalRect;
	public GameObject pedestalTri;

	public GameObject mark;
	public Light myLight;
	public Light myLight2;
	public GameObject myTeleport;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		/*
		if (Input.GetKeyDown (KeyCode.W)){ 			
			pedestalCube.transform.parent.transform.DOMoveY (-1.5f, 12f);
			pedestalCube.transform.DOShakePosition (12f, .005f, 5, 5f);

			pedestalRect.transform.parent.transform.DOMoveY (-1.5f, 12f);
			pedestalRect.transform.DOShakePosition (12f, .005f, 5, 5f);

			pedestalTri.transform.parent.transform.DOMoveY (-1.5f, 12f);
			pedestalTri.transform.DOShakePosition (12f, .005f, 5, 5f);
			//CompletePuzzle ();
		}
		*/

		if (!puzzleComplete) {
			if (pedestalCube.GetComponent<BowPedestalObjective> ().pedestalActive &&
			    pedestalRect.GetComponent<BowPedestalObjective> ().pedestalActive &&
			    pedestalTri.GetComponent<BowPedestalObjective> ().pedestalActive) {
				CompletePuzzle ();
			}
		}
	}

	public void CompletePuzzle (){
		if (!puzzleComplete) {
			puzzleComplete = true;
			mark.SetActive (false);


			myTeleport.GetComponent<Valve.VR.InteractionSystem.TeleportPoint> ().markerActive = false;
		}

	}


	public void pickBow(){
		Debug.Log ("pickBow");
		safeZone.enabled = true;
	}

	public void releaseBow(){
		Debug.Log ("releaseBow");
		safeZone.enabled = false;
	}


	public void DisableLight(){
		myLight.enabled = false;
		myLight2.enabled = false;
	}
}
