﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Valve.VR.InteractionSystem;

public class BowPedestalArea : MonoBehaviour {

	public GameObject objectToTrack;
	public BowPedestalObjective pedestal;
	public SoundPlayOneshot pedestalSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay (Collider other){
		//Debug.Log ("stay " + other.name);
	}

	void OnTriggerEnter (Collider other){

		if (other.transform.parent != null) {
			if (other.transform.parent.gameObject == objectToTrack) {
				Debug.Log ("enter " + other.name);
				pedestal.pedestalActive = false;
			}
		}
	}

	void OnTriggerExit (Collider other){
		if (other.transform.parent != null) {
			if (other.transform.parent.gameObject == objectToTrack) {
				Debug.Log ("exit " + other.name);
				pedestal.pedestalActive = true;

				pedestal.transform.parent.transform.DOMoveY (-1.5f, 12f);
				pedestal.transform.DOShakePosition (12f, .005f, 5, 5f);
				if ( pedestalSound != null )
				{
					pedestalSound.Play();
				}
				Destroy (this);
			}
		}
	}

}
