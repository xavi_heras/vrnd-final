﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Valve.VR.InteractionSystem;

public class WheelPuzzleController : MonoBehaviour {
	public GameObject rectangle;
	public GameObject cube;
	public GameObject triangle;
	private float angleMargin = 15f;
	public bool rectOK = false;
	public bool cubeOK = false;
	public bool triOK = false;
	public bool puzzleComplete = false;
	public GameObject pedestal;
	public bool movingWheel = false;

	public GameObject mark;
	public Light myLight;
	public Light myLight2;
	public GameObject myTeleport;
	public GameObject sandClue;
	public SoundPlayOneshot sandSound;

	public Material baseMate;
	public SoundPlayOneshot pushRock;
	// Use this for initialization
	void Start () {
		float randomAngle = 0;
		bool angleOK = false;

		//rectangle angle
		while (!angleOK) {
			angleOK = true;
			randomAngle = Random.Range (-180, 180);
			if (randomAngle >= 90 + angleMargin*2 && randomAngle <= 90 - angleMargin*2)
				angleOK = false;
			if (randomAngle >= -90 + angleMargin*2 && randomAngle <= -90 - angleMargin*2)
				angleOK = false;
		}
		rectangle.transform.Rotate (0, 0, randomAngle);

		//cube angle
		angleOK  = false;
		while (!angleOK) {
			angleOK = true;
			randomAngle = Random.Range (-180, 180);
			if (randomAngle >= 45 + angleMargin*2 && randomAngle <= 45 - angleMargin*2)
				angleOK = false;
			if (randomAngle >= -45 + angleMargin*2 && randomAngle <= -45 - angleMargin*2)
				angleOK = false;
			if (randomAngle >= 130 + angleMargin*2 && randomAngle <= 130 - angleMargin*2)
				angleOK = false;
			if (randomAngle >= -130 + angleMargin*2 && randomAngle <= -130 - angleMargin*2)
				angleOK = false;
		}
		cube.transform.Rotate (0, 0, randomAngle);

		//triangle angle
		angleOK  = false;
		while (!angleOK) {
			angleOK = true;
			randomAngle = Random.Range (-180, 180);
			if (randomAngle >= 90 + angleMargin*2 && randomAngle <= 90 - angleMargin*2)
				angleOK = false;
			if (randomAngle >= -90 + angleMargin*2 && randomAngle <= -90 - angleMargin*2)
				angleOK = false;
		}
		triangle.transform.Rotate (0, 0, randomAngle);
	}
	
	// Update is called once per frame
	void Update () {

		//if (Input.GetKeyDown (KeyCode.E)) CompletePuzzle ();

		float margin = 0.025f;
		//float angle90 = 0.7070344f;
		//float angle45 = 0.3826628f;
		//float angle130 = 0.9062591f;

		float angle90 = 0.55f;
		float angle45 = 0.3826628f;
		float angle130 = 0.55f;

		//Debug.Log ("rec: " + rectangle.transform.rotation.z + " cub: " + cube.transform.rotation.z + " tri: " + triangle.transform.rotation.z);
		if (!puzzleComplete) {
			//rectangle angle
			if (rectangle.GetComponent<Valve.VR.InteractionSystem.CircularDrive> ().handHoverLocked == null) {			
				if (rectangle.transform.rotation.z >= angle90 - margin && rectangle.transform.rotation.z <= angle90 + margin)
					rectOK = true;
				else if (rectangle.transform.rotation.z >= -angle90 - margin && rectangle.transform.rotation.z <= -angle90 + margin)
					rectOK = true;
				else
					rectOK = false;
			} else {
				rectOK = false;
			}

			//cube angle
			if (cube.GetComponent<Valve.VR.InteractionSystem.CircularDrive> ().handHoverLocked == null) {
				if (cube.transform.rotation.z >= .30f - margin && cube.transform.rotation.z <= .30f + margin)
					cubeOK = true;
				else if (cube.transform.rotation.z >= -.30f - margin && cube.transform.rotation.z <= -.30f+ margin)
					cubeOK = true;
				else if (cube.transform.rotation.z >= .72f - margin && cube.transform.rotation.z <= .72f+ margin)
					cubeOK = true;
				else if (cube.transform.rotation.z >= -.72f - margin && cube.transform.rotation.z <= -.72f+ margin)
					cubeOK = true;
				else
					cubeOK = false;
			} else {
				cubeOK = false;
			}

			//triangle angle
			if (triangle.GetComponent<Valve.VR.InteractionSystem.CircularDrive> ().handHoverLocked == null) {
				if (triangle.transform.rotation.z >= -angle90 - margin && triangle.transform.rotation.z <= -angle90 + margin)
					triOK = true;
				else if (triangle.transform.rotation.z >= angle90 - margin && triangle.transform.rotation.z <= angle90 + margin)
					triOK = true;
				else
					triOK = false;
			} else {
				triOK = false;
			}

			if (rectOK && cubeOK && triOK) {

				CompletePuzzle ();



			} else
				puzzleComplete = false;
		}
	}

	public void  CompletePuzzle (){
		if (!puzzleComplete){
			Debug.Log ("wheel puzzle complete!");

			mark.SetActive (false);

			puzzleComplete = true;
			rectangle.GetComponentInChildren<MeshRenderer> ().material = baseMate;
			Destroy (rectangle.GetComponent<Valve.VR.InteractionSystem.CircularDrive> ());
			Destroy (rectangle.GetComponent<Valve.VR.InteractionSystem.InteractableHoverEvents> ());
			Destroy (rectangle.GetComponent<Valve.VR.InteractionSystem.Interactable> ());


			cube.GetComponentInChildren<MeshRenderer> ().material = baseMate;
			Destroy (cube.GetComponent<Valve.VR.InteractionSystem.CircularDrive> ());
			Destroy (cube.GetComponent<Valve.VR.InteractionSystem.InteractableHoverEvents> ());
			Destroy (cube.GetComponent<Valve.VR.InteractionSystem.Interactable> ());

			triangle.GetComponentInChildren<MeshRenderer> ().material = baseMate;
			Destroy (triangle.GetComponent<Valve.VR.InteractionSystem.CircularDrive> ());
			Destroy (triangle.GetComponent<Valve.VR.InteractionSystem.InteractableHoverEvents> ());
			Destroy (triangle.GetComponent<Valve.VR.InteractionSystem.Interactable> ());

			//pedestal.transform.parent.transform.DOMoveY (-1.5f, 12f);
			//pedestal.transform.DOShakePosition (12f, .005f, 5, 5f);

			if ( pushRock!= null )
				pushRock.Play();

			rectangle.transform.DOLocalMoveZ (-.432f, 1f, false).OnComplete(() => {
				if ( pushRock!= null )
					pushRock.Play();
				
				cube.transform.DOLocalMoveZ (-.47f, 1f, false).OnComplete(() => {
					if ( pushRock!= null )
						pushRock.Play();

					triangle.transform.DOLocalMoveZ (-.498f, 1f, false).OnComplete(() => {

						sandClue.transform.DOMoveY (-7.25f, 20f);
						if ( sandSound != null )
						{
							sandSound.Play();
						}


						myTeleport.GetComponent<Valve.VR.InteractionSystem.TeleportPoint> ().markerActive = false;

					});});
			});


		}
	}

	public void DisableLight(){
		myLight.enabled = false;
		myLight2.enabled = false;
	}

	public void GrabWheel(){
		Debug.Log ("GrabWheel");
		movingWheel = true;
	}

	public void ReleaseWheel(){
		movingWheel = false;
		Debug.Log ("ReleaseWheel");
	}
}
