﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetToyPosition : MonoBehaviour {

	public Transform origin;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance (this.transform.position, origin.position) > 1) {
			this.transform.position = origin.position;
			this.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		}
		//Debug.Log (Vector3.Distance(this.transform.position, origin.position).ToString());
	}
}
